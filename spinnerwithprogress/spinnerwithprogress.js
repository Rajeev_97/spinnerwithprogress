import { LightningElement, track } from 'lwc';

export default class Spinnerwithprogress extends LightningElement {
    progress = 0;
    processStatus = 'In Progress';
    connectedCallback() {
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this._interval = setInterval(() => {
            this.progress = this.progress + 5;
            this.processStatus = 'Processing => ' + this.progress +'/100';
            if(this.progress === 100) {
                clearInterval(this._interval);
                this.processStatus = 'Completed';
            }
        }, 300);

    }
}